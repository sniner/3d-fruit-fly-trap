# Fruit fly trap

This is a trap for fruit flies (Drosophila), which are attracted by fruit
lying around openly.

Usage: Get a cup, tumbler or jar made of glass or transparent plastic.
Customize the dimensions of the fruit fly trap to fit the opening and
adjust the height to about two thirds of the inner height of the jar.

Place a small piece of a ripe banana at the bottom of the glass. In my
experience, bananas work very well with fruit flies. Close the jar with
the fruit fly trap.

![](./demo1.png)

The fruit flies are attracted by the scent of the banana, crawl through
the small opening in the jar to get to the banana, but then can't find
their way out of the jar. The next day, or at the latest when the banana
has dried up, take the jar and open it outside to release the fruit flies
there. If you are inclined to kill the fruit flies, you can put some
dishwashing liquid on the trap and fill the glass with water.

Published on [Thingiverse][1] on 19.07.2020.

License: [CC BY-SA 4.0][2]

[1]: https://www.thingiverse.com/thing:4546202
[2]: https://creativecommons.org/licenses/by-sa/4.0/